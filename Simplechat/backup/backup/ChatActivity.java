package pure.simplechat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;

/**
 * Created by Nanda on 6/26/2016.
 */
public class ChatActivity extends AppCompatActivity {

    EditText etMessage;
    Button btSend;
    Firebase firebasedat;
    Firebase userRef;
    ListView chatlist;
    ChildEventListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);

        Firebase.setAndroidContext(this);
        firebasedat = new Firebase("https://simplechat-395c5.firebaseio.com/");

        etMessage = (EditText) findViewById(R.id.etMessage);
        btSend = (Button) findViewById(R.id.btSend);
        chatlist = (ListView) findViewById(R.id.lvChat);
        String[] testval = new String[] {
                "Ab",
                "AA"
        };

        final ArrayList<String> chatmsgs = new ArrayList<String>();
        final ArrayAdapter chatadapter = new ChatAdapter(this, chatmsgs);
        chatlist.setAdapter(chatadapter);
        mListener = this.firebasedat.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
                userRef = firebasedat.child("users");
                btSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String message = etMessage.getText().toString();
                        userRef.push().setValue(message);
                        chatmsgs.add(message);
                        //chatmsgs.add(message);


                        chatadapter.notifyDataSetChanged();



                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        /*btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userRef = firebasedat.child("users");
                String message = etMessage.getText().toString();
                userRef.push().setValue(message);
                chatmsgs.add(message);
                chatadapter.notifyDataSetChanged();

            }
        });*/
    }
}

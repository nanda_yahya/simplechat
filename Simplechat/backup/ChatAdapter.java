package pure.simplechat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Nanda on 6/24/2016.
 */
public class ChatAdapter extends ArrayAdapter<String> {

    public ChatAdapter(Context context, String[] nama) {
        super(context, R.layout.chatlist, nama);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater nandainflater = LayoutInflater.from(getContext());
        View customView = nandainflater.inflate(R.layout.chat, parent, false);

        String singleNameItem = getItem(position);
        TextView userTextView = (TextView) customView.findViewById(R.id.nandatextview);
        ImageView userImage = (ImageView) customView.findViewById(R.id.nandaimage);

        userTextView.setText(singleNameItem);
        userImage.setImageResource(R.drawable.ic_video_youtube);
        return customView;

    }
}

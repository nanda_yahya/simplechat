package pure.simplechat;

/**
 * Created by Nanda on 6/24/2016.
 */
public class Chat {

    private String username;
    private String message;

    public Chat() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

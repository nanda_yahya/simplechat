package pure.simplechat;

import android.app.Application;

import com.firebase.client.Firebase;

public class Simplechat extends Application {

    public static final String TAG = "Simplechat";


    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}


package pure.simplechat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Nanda on 7/11/2016.
 */
public class Contact extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);
        setTitle("Contact List");

        String users[] = {"nanda@yahya.com","angga@pure.com","egi","agus"};
        final ListView contactlist = (ListView)findViewById(R.id.contact_list);
        final ArrayAdapter useradapter= new ArrayAdapter(this, android.R.layout.simple_list_item_1, users);
        contactlist.setAdapter(useradapter);

        final Intent intent = getIntent();
        final String userid = intent.getStringExtra("userID");

        contactlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                               @Override
                                               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    String userpos = contactlist.getItemAtPosition(position).toString();

                                                   Intent userintent = new Intent();
                                                   userintent.setClass(Contact.this, MainActivity.class);
                                                   userintent.putExtra("userIDC", userid);
                                                   userintent.putExtra("contactID", userpos);

                                                   Toast.makeText(Contact.this, "You are now chatting with " + userpos, Toast.LENGTH_LONG).show();
                                                   startActivity(userintent);
                                               }
                                           }
        );
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Keluar")
                .setMessage("Apakah anda yakin ingin keluar?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("Tidak", null).show();
    }
}

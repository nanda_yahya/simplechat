package pure.simplechat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nanda on 7/5/2016.
 */
public class Login extends ActionBarActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final Firebase sRef = new Firebase("https://simplechat-395c5.firebaseio.com");
    private static final Firebase sRefChild = sRef.child("UserDB");
    private static final Firebase sRefPush = sRefChild.push();
    //private Query queryref = sRef.orderByChild("username");

    EditText txtID;
    EditText txtPassword;
    Button btnLogin;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        setTitle("Login");

        mAuth = FirebaseAuth.getInstance();
        txtID = (EditText) findViewById(R.id.txtID);
        txtPassword = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        txtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("NANDA", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("NANDA", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin: //BUTTON LOGIN
            final String Logininfo = txtID.getText().toString();
                if (txtID.getText().toString().length() == 0) {
                    Toast.makeText(Login.this, "ID tidak boleh kosong", Toast.LENGTH_SHORT).show();

                } else if (txtPassword.getText().toString().length() == 0) {
                    Toast.makeText(Login.this, "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();

                } else {
                /*sRef.authWithPassword(txtID.getText().toString(), txtPassword.getText().toString(), new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        Intent loginIntent = new Intent();
                        loginIntent.setClass(Login.this, Contact.class);
                        loginIntent.putExtra("userID", Logininfo);
                        startActivity(loginIntent);

                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                    Log.d("ERROR", firebaseError.toString());
                    }

                });*/
                    mAuth.signInWithEmailAndPassword(txtID.getText().toString(), txtPassword.getText().toString())
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Log.w("SUCCESSERROR", "signInWithEmail", task.getException());
                                        Toast.makeText(Login.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.d("SUCCESS", "signInWithEmail:onComplete:" + task.isSuccessful());
                                        Intent loginIntent = new Intent();
                                        loginIntent.setClass(Login.this, Contact.class);
                                        loginIntent.putExtra("userID", Logininfo);
                                        startActivity(loginIntent);

                                    }

                                    // ...
                                }
                            });




                    //Intent loginIntent = new Intent(getApplicationContext(),MainActivity.class);

                    //Intent loginIntent = new Intent();
                    //loginIntent.setClass(Login.this, Contact.class);
                    //loginIntent.putExtra("userID", Logininfo);
                    //startActivity(loginIntent);

                }
                break;
            case R.id.btnRegister: //BUTTON REGISTER

                //Query queryref = sRefChild.orderByChild("username").equalTo(txtID.getText().toString());

                //final String uniqueID = sRefPush.getKey();
                //Firebase uniqueIDuser = sRef.child("UserDB").child(uniqueID).child("username");

                if (txtID.getText().toString().length() == 0 && txtPassword.getText().toString().length() == 0) {
                    Toast.makeText(Login.this, "Username dan password tidak boleh kosong", Toast.LENGTH_SHORT).show();

                } else if (txtID.getText().toString().length() == 0) {
                    Toast.makeText(Login.this, "ID tidak boleh kosong", Toast.LENGTH_SHORT).show();

                } else if (txtPassword.getText().toString().length() == 0) {
                    Toast.makeText(Login.this, "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                //} else if (uniqueID == txtID.getText().toString()){
                   // Toast.makeText(Login.this, "Username telah diambil", Toast.LENGTH_SHORT).show();
                    } else {

                    mAuth.createUserWithEmailAndPassword(txtID.getText().toString(), txtPassword.getText().toString())    //METHOD BARU!!!
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d("NANDA", "createUserWithEmail:onComplete:" + task.isSuccessful());


                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(Login.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(Login.this, "Successfully registered", Toast.LENGTH_LONG).show();
                                    }

                                    // ...
                                }
                            });


                    /*Map<String, String> userpass = new HashMap<String, String>();
                    userpass.put("username", txtID.getText().toString());
                    userpass.put("pass", txtPassword.getText().toString());
                    sRefPush.setValue(userpass, new Firebase.CompletionListener(){
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            if (firebaseError != null) {
                                Toast.makeText(Login.this, "Registrasi gagal" + firebaseError.getMessage(), Toast.LENGTH_SHORT ).show();
                            } else {
                                Toast.makeText(Login.this, "Registrasi sukses", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });*/

                }
                /*Intent sourceintent = new Intent(Login.this, MessageDataSource.class);
                sourceintent.putExtra("userIDSource", Logininfo);
                startActivity(sourceintent);*/
                }

        }

    }


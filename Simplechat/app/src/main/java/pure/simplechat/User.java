package pure.simplechat;

/**
 * Created by Nanda on 7/20/2016.
 */
public class User {

    private String email;
    private String password;
    private String username;


    public User(String username, String email) {
        this.email = email;
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
